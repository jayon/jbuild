'use strict'

var fs = require('fs')
  , mkdirp = require('mkdirp')
  , path = require('path')
  , jade = require('jade')
  ;

function Walker(config) {
  var self = this;

  config = config || {};

  self.re = config.filter || /.*/;

  self.origin = config.origin;

  self.onFile = config.onFile;

  self.timeout;

  self.walk = function(uri) {

    clearTimeout(self.timeout);
    self.timeout = setTimeout(function() {
      if (typeof config.callback === 'function') {
        config.callback();
      }
    }, 1000);

    fs.lstat(uri, function(err, stat) {
      if (err) {
        throw err;
      }

      if (stat.isFile() && self.re.test(path.basename(uri))) {
        if (typeof self.onFile !== 'function') {
          return;
        }

        fs.readFile(uri, 'utf8', function(err, str) {
          if (err) {
            throw err;
          }

          self.onFile({
            str: str,
            uri: uri,
            dir: path.dirname(uri),
            name: path.basename(uri)
          });
        });
      }

      else if (stat.isDirectory()) {
        fs.readdir(uri, function(err, files) {
          if (err) {
            throw err;
          }

          files.map(function(filename) {
            return uri + '/' + filename;
          })
          .forEach(self.walk);
        });
      }
    });
  }

  return self;
}

function Compiler() {
  var self = this;

  self.jade = function(opt, callback) {
    opt = opt || {};

    opt.options = opt.options || {};

    if (!opt.src && !opt.dest) {
      throw 'Define src and dest';
    }

    var w = new Walker({
      callback: callback,

      filter: opt.filter || /^(?!_)/,

      onFile: function(e) {
        var fn, dest;

        opt.options.filename = e.uri;

        fn = jade.compile(e.str, opt.options);

        dest = path.join(opt.dest, e.dir.split(opt.src)[1]);

        // Creo estructura de directorios segun la fuente
        mkdirp(dest, '0755', function(err) {
          if (err) {
            throw err;
          }

          fs.writeFile(path.join(dest, e.name.split('.jade').join('.html')), fn(opt.options), function(err) {
            if (err) {
              throw err;
            }
          });
        }); 
      }
    });

    if (typeof opt.src === 'object' && opt.src.length > 0) {
      opt.src.forEach(function(uri) {
        w.walk(uri);
      });
    }
  };

  return self;
}

exports.Walker = Walker;
exports.Compiler = Compiler;

