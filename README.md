# Gruntjs

```javascript
var cc = require('jbuild');

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    ...

    jbuild: {
      admin: {
        filter: /^(?!_|base|layout)/,
        src: [ 'templates/' ],
        dest: 'build',
        options: { client: false, pretty: true }
      }
    }
  })

  grunt.registerMultiTask('jbuild', 'go!', function() {
    var self = this;
    var c = new cc.Compiler();
    var done = this.async();

    c.jade({
      src: self.data.src,
      dest: self.data.dest,
      options: self.data.options
    }, function() {
      done(true);
    });
  });

};
```
